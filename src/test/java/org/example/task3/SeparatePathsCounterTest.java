package org.example.task3;

import com.google.common.graph.GraphBuilder;
import com.google.common.graph.ImmutableGraph;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class SeparatePathsCounterTest {

	@Test
	void test() {
		ImmutableGraph<Integer> graph =
				GraphBuilder.undirected().<Integer>immutable()
						.putEdge(1, 2)
						.putEdge(2, 3)
						.putEdge(5, 6)
						.build();

		SeparatePathsCounter counter = new SeparatePathsCounter(graph);

		Assertions.assertEquals(2, counter.count());
	}

}