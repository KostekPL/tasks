package org.example.task3;

import com.google.common.graph.EndpointPair;
import com.google.common.graph.Graph;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.HashSet;

class IntegerGraphScannerTest {

	private static final InputStream DEFAULT_SIN = System.in;

	@AfterAll
	static void tearDown() {
		System.setIn(DEFAULT_SIN);
	}

	@Test
	void shouldProperlyBuildsGraphFromInput() {
		ByteArrayInputStream input = new ByteArrayInputStream("3\n1 2\n2 3\n3 4".getBytes());
		System.setIn(input);

		IntegerGraphScanner graphScanner = new IntegerGraphScanner();
		Graph<Integer> graph = graphScanner.read();

		Assertions.assertEquals(3, graph.edges().size());
		Assertions.assertEquals(new HashSet<EndpointPair<Integer>>() {{
			add(EndpointPair.unordered(1, 2));
			add(EndpointPair.unordered(2, 3));
			add(EndpointPair.unordered(3, 4));
		}}, graph.edges());
	}

	@Test
	void shouldThrowsIllegalArgumentExceptionWhenUnreadableInputLineForEdge() {
		IntegerGraphScanner graphScanner = new IntegerGraphScanner();
		String unreadableEdgeLine = "1 2 3";

		Assertions.assertThrows(IllegalArgumentException.class, () -> graphScanner.readEdge(unreadableEdgeLine));
	}

}