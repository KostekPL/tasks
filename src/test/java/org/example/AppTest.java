package org.example;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;

class AppTest {

	@Test
	void task1ExecutionTest() {
		App.main(new String[]{"task1"});
		App.main(new String[]{"task1","5"});
		App.main(new String[]{"task1","5","10"});
	}

	@Test
	void task2ExecutionTest() {
		App.main(new String[]{"task2"});
		App.main(new String[]{"task2","10"});
		App.main(new String[]{"task2","10","10"});
		App.main(new String[]{"task2","10","10","10"});
	}

	@Test
	void task3ExecutionTest() {
		ByteArrayInputStream input = new ByteArrayInputStream("3\n1 2\n2 3\n3 4".getBytes());
		System.setIn(input);
		App.main(new String[]{"task3"});
	}

}