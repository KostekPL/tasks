package org.example;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

class RandIntGeneratorTest {

	@Test
	void generate() {
		List<Integer> generated = RandIntGenerator.generate(100, 50);

		Assertions.assertEquals(100, generated.size());
		Assertions.assertTrue(generated.stream()
				.noneMatch(x -> x > 50));
		Assertions.assertTrue(generated.stream()
				.noneMatch(x -> x < 0));
	}
}