package org.example;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

class CliArgsHelperTest {

	private static Stream<Arguments> provideValuesForCreateMethod() {
		return Stream.of(
				Arguments.of(new String[]{"500", "100"}, 0, 0, 500),
				Arguments.of(new String[]{"500", "100"}, 1, 0, 100),
				Arguments.of(null, 0, 500, 500)
		);
	}

	@ParameterizedTest
	@MethodSource("provideValuesForCreateMethod")
	void shouldProperlyParseCliArgumentOrChooseDefault(String[] args, int index, int defaultVal, int expected) {

		int result = CliArgsHelper.readArgAsInt(args, index, defaultVal);

		Assertions.assertEquals(expected, result);
	}
}