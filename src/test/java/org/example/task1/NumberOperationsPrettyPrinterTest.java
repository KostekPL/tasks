package org.example.task1;

import org.example.SystemOutTest;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

class NumberOperationsPrettyPrinterTest extends SystemOutTest {

	private final NumberOperationsPrettyPrinter prettyPrint = new NumberOperationsPrettyPrinter();

	@Test
	void shouldProperlyPrintDistinctNumbers() {
		prettyPrint.printNumbers(Arrays.asList(5, 4, 7, 1, 3, 21));

		assertSystemOutEquals("5 4 7 1 3 21 ");
	}

	@Test
	void shouldProperlyPrintExpectingOutput() {
		prettyPrint.printAll(new NumberOperations(Arrays.asList(1, 10, 20, 20, 2, 5)));

		assertSystemOutEquals("1 2 5 10 20 \r\ncount: 6 \r\ndistinct: 5 \r\nmin: 1 \r\nmax: 20");
	}

}