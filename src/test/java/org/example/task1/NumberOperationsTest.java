package org.example.task1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

class NumberOperationsTest {

	private final NumberOperations t1 = new NumberOperations(Arrays.asList(5, 7, 11, -50, 51, 11, 11, 11, 11));

	@Test
	void shouldProperlyFindMaxValue() {
		Assertions.assertEquals(51, t1.getMax());
	}

	@Test
	void shouldProperlyFindMinValue() {
		Assertions.assertEquals(-50, t1.getMin());
	}

	@Test
	void shouldProperlyCount() {
		Assertions.assertEquals(9, t1.getCount());
	}

	@Test
	void shouldProperlyCountDistinct() {
		Assertions.assertEquals(5, t1.getDistinctCount());
	}
}