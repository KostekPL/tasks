package org.example;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public abstract class SystemOutTest {

	private static final PrintStream DEFAULT_SOUT = System.out;
	private static final ByteArrayOutputStream TEST_SOUT = new ByteArrayOutputStream();

	@BeforeAll
	static void setUp() {
		System.setOut(new PrintStream(TEST_SOUT));
	}

	@AfterAll
	static void tearDown() {
		System.setOut(DEFAULT_SOUT);
	}

	@BeforeEach
	void beforeEach() {
		TEST_SOUT.reset();
	}

	protected void assertSystemOutEquals(String expected) {
		Assertions.assertEquals(expected, TEST_SOUT.toString());
	}

}