package org.example;

import org.example.task1.Task1;
import org.example.task2.Task2;
import org.example.task3.Task3;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

class TaskFactoryTest {

	private static Stream<Arguments> provideValuesForCreateTaskMethod() {
		return Stream.of(
				Arguments.of("task1", Task1.class),
				Arguments.of("task2", Task2.class),
				Arguments.of("task3", Task3.class)
		);
	}

	@ParameterizedTest
	@MethodSource("provideValuesForCreateTaskMethod")
	void shouldChooseRightTaskImplementation(String value, Class<?> expected) {

		Task result = TaskFactory.createTask(value);
		Assertions.assertEquals(expected, result.getClass());
	}

	@Test
	void shouldThrowsExceptionWhenNotFoundTask() {

		Assertions.assertThrows(IllegalArgumentException.class, () -> TaskFactory.createTask(null));
		Assertions.assertThrows(IllegalArgumentException.class, () -> TaskFactory.createTask("unknown"));
	}
}