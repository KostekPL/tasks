package org.example.task2;

import org.example.SystemOutTest;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

class SumFinderTest extends SystemOutTest {

	@Test
	void shouldProperlyPrintExpectingOutputForOddSum() {

		new SumFinder().find(Arrays.asList(1, 2, 10, 7, 5, 3, 6, 6, 13, 0), 13);

		assertSystemOutEquals("0 13\r\n3 10\r\n6 7\r\n6 7\r\n");
	}

	@Test
	void shouldProperlyPrintExpectingOutputForEvenSum() {

		new SumFinder().find(Arrays.asList(1, 2, 10, 7, 5, 3, 6, 6, 13, 0), 12);

		assertSystemOutEquals("2 10\r\n5 7\r\n");
	}

}