package org.example.task1;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

class NumberOperations {

	private final List<Integer> input;
	private final List<Integer> distinct;

	NumberOperations(List<Integer> input) {
		this.input = input;
		distinct = new ArrayList<>(new HashSet<>(this.input));
		distinct.sort(Integer::compareTo);
	}

	long getCount() {
		return input.size();
	}

	List<Integer> getDistinct() {
		return distinct;
	}

	long getDistinctCount() {
		return distinct.size();
	}

	int getMax() {
		return distinct.get(distinct.size() - 1);
	}

	int getMin() {
		return distinct.get(0);
	}

}