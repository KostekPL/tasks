package org.example.task1;

import java.util.List;

class NumberOperationsPrettyPrinter {

	private static final String SPACE = " ";

	void printAll(NumberOperations no) {
		printNumbers(no.getDistinct());
		System.out.println();
		System.out.println("count:" + SPACE + no.getCount() + SPACE);
		System.out.println("distinct:" + SPACE + no.getDistinctCount() + SPACE);
		System.out.println("min:" + SPACE + no.getMin() + SPACE);
		System.out.print("max:" + SPACE + no.getMax());
	}

	void printNumbers(List<Integer> list) {
		list.forEach(x -> System.out.print(x + SPACE));
	}
}
