package org.example.task1;

import org.example.CliArgsHelper;
import org.example.RandIntGenerator;
import org.example.Task;

public class Task1 implements Task {

	private static final int DEFAULT_NUMBER_COUNT = 100000;
	private static final int DEFAULT_NUMBER_RANGE = 500;

	@Override
	public void execute(String... args) {
		int numberCount = CliArgsHelper.readArgAsInt(args, 0, DEFAULT_NUMBER_COUNT);
		int numberRangeExclusive = CliArgsHelper.readArgAsInt(args, 1, DEFAULT_NUMBER_RANGE);
		NumberOperations no = new NumberOperations(RandIntGenerator.generate(numberCount, numberRangeExclusive));
		new NumberOperationsPrettyPrinter().printAll(no);
	}

}
