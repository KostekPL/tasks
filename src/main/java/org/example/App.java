package org.example;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;

@Slf4j
public class App {

	public static void main(String[] args) {
		long startTimeInMillis = System.currentTimeMillis();

		Task task = TaskFactory.createTask(ArrayUtils.get(args, 0));
		task.execute(ArrayUtils.subarray(args, 1, args.length));

		log.info(String.format("Execution has taken %s ms", System.currentTimeMillis() - startTimeInMillis));
	}
}
