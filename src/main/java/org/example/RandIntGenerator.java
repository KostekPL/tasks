package org.example;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class RandIntGenerator {

	private static final Random RAND = new Random();

	public static List<Integer> generate(int count, int maxValue) {
		return IntStream.range(0, count)
				.mapToObj(x -> RAND.nextInt(maxValue + 1))
				.collect(Collectors.toList());
	}
}
