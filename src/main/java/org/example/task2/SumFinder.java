package org.example.task2;

import lombok.RequiredArgsConstructor;

import java.util.Arrays;
import java.util.List;

@RequiredArgsConstructor
class SumFinder {

	private static final String SPACE = " ";

	void find(List<Integer> list, int expectingSum) {
		Integer[] arr = list.toArray(new Integer[0]);

		Arrays.sort(arr);

		int indexOfFirstHigherThanHalf = indexOfFirstHigherThanHalf(arr, expectingSum);

		if (indexOfFirstHigherThanHalf < 0) {
			return;
		}

		Integer[] lowest = Arrays.copyOfRange(arr, 0, indexOfFirstHigherThanHalf);
		Integer[] highest = Arrays.copyOfRange(arr, indexOfFirstHigherThanHalf, arr.length);

		for (Integer low : lowest) {
			int searched = expectingSum - low;
			int indexOfSearched = Arrays.binarySearch(highest, searched);
			if (indexOfSearched > -1) {
				printNumbers(low, searched);
			}
		}
	}

	private int indexOfFirstHigherThanHalf(Integer[] arr, int range) {
		int checked;
		if (range % 2 == 1) {
			checked = (int) Math.ceil(range / 2.0);
		} else {
			checked = range / 2 + 1;
		}
		int indexOf = -1;
		while (indexOf < 0 && checked <= range) {
			indexOf = Arrays.binarySearch(arr, checked);
			checked++;
		}
		return indexOf;
	}


	void printNumbers(int number1, int number2) {
		System.out.println(number1 + SPACE + number2);
	}

}
