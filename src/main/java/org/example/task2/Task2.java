package org.example.task2;

import org.example.CliArgsHelper;
import org.example.RandIntGenerator;
import org.example.Task;

public class Task2 implements Task {

	private static final int DEFAULT_NUMBER_COUNT = 100000;
	private static final int DEFAULT_NUMBER_RANGE = 500;
	private static final int DEFAULT_EXPECTED_SUM = 501;

	@Override
	public void execute(String... args) {
		int numberCount = CliArgsHelper.readArgAsInt(args, 0, DEFAULT_NUMBER_COUNT);
		int numberRangeExclusive = CliArgsHelper.readArgAsInt(args, 1, DEFAULT_NUMBER_RANGE);
		int expectedSum = CliArgsHelper.readArgAsInt(args, 2, DEFAULT_EXPECTED_SUM);

		new SumFinder().find(RandIntGenerator.generate(numberCount, numberRangeExclusive), expectedSum);
	}
}
