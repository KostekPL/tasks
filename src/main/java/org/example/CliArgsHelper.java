package org.example;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

public class CliArgsHelper {

	public static int readArgAsInt(String[] args, int index, Integer defaultValue) {
		String s = ArrayUtils.get(args, index);
		if (StringUtils.isBlank(s)) {
			return defaultValue;
		} else {
			return Integer.parseInt(s);
		}
	}
}
