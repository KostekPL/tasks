package org.example;

import org.example.task1.Task1;
import org.example.task2.Task2;
import org.example.task3.Task3;

class TaskFactory {

	static Task createTask(String task) {
		if ("task1".equals(task)) {
			return new Task1();
		} else if ("task2".equals(task)) {
			return new Task2();
		} else if ("task3".equals(task)) {
			return new Task3();
		}
		throw new IllegalArgumentException("Unknown task name, allowed values: task1|task2|task3");
	}
}
