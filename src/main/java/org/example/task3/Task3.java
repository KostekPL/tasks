package org.example.task3;

import org.example.Task;

public class Task3 implements Task {

	@Override
	public void execute(String... args) {
		System.out.println(new SeparatePathsCounter(new IntegerGraphScanner().read()).count());
	}
}
