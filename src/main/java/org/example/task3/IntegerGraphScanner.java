package org.example.task3;

import com.google.common.graph.Graph;
import com.google.common.graph.GraphBuilder;
import com.google.common.graph.ImmutableGraph;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class IntegerGraphScanner {

	private static final String NUMBER_1_GROUP_NAME = "number1";
	private static final String NUMBER_2_GROUP_NAME = "number2";
	private static final String REGEX = "(?<" + NUMBER_1_GROUP_NAME + ">\\d+)\\ (?<" + NUMBER_2_GROUP_NAME + ">\\d+)";

	private static final Pattern PATTERN = Pattern.compile(REGEX);

	private final Scanner s = new Scanner(System.in);
	private final ImmutableGraph.Builder<Integer> builder = GraphBuilder.undirected().immutable();

	Graph<Integer> read() {
		int graphSize = s.nextInt();
		s.nextLine();
		for (int i = 0; i < graphSize; i++) {
			readEdge(s.nextLine());
		}
		return builder.build();
	}

	void readEdge(String edge) {
		Matcher matcher = PATTERN.matcher(edge);
		if (matcher.matches()) {
			int firstNumber = parseToInt(matcher.group(NUMBER_1_GROUP_NAME));
			int secondNumber = parseToInt(matcher.group(NUMBER_2_GROUP_NAME));
			builder.putEdge(firstNumber, secondNumber);
		} else {
			throw new IllegalArgumentException("Invalid edge format. Expected: Vertex(space)Vertex.");
		}
	}

	private int parseToInt(String str) {
		return Integer.parseInt(str);
	}
}
