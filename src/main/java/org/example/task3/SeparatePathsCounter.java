package org.example.task3;

import com.google.common.graph.Graph;
import com.google.common.graph.Traverser;

import java.util.HashSet;
import java.util.Set;

class SeparatePathsCounter {

	private final Graph<Integer> exploredGraph;
	private final Traverser<Integer> traverser;

	SeparatePathsCounter(final Graph<Integer> exploredGraph) {
		this.exploredGraph = exploredGraph;
		this.traverser = Traverser.forGraph(exploredGraph);
	}

	int count() {
		int count = 0;
		Set<Integer> visited = new HashSet<>();
		for (Integer node : exploredGraph.nodes()) {
			if (visited.contains(node)) {
				continue;
			}
			count++;
			traverser.depthFirstPostOrder(node).forEach(visited::add);
		}
		return count;
	}

}
