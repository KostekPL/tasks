# DEV-TEST-RESULTS #

### From author ###
At the beginning I have to thank, in daily routine I don't pay much attention to optimal solutions. So it was interesting experience to do these tasks.

### About project ###

I decided to pack all tasks in one maven project. 

To generate jar just run `mvn package`

generated jar should appears in target directory.

It's important to use `Task-1.0-jar-with-dependencies.jar` to execute below tasks.

### Task1 ###
Numbers in this task are generated automatically. You have to only run jar file with task1 argument. You can also pass optional arguments (read about it below)
* To run task1 execute `java -jar Task-1.0-jar-with-dependencies.jar task1` terminal command.
* There are two more optional arguments expected:


	Number Count - How many numbers should be passed to task - if not passed then 100000
	Number Range - Generated numbers are in range 0<=x<=thisParamValue - if not passed then 500
* Example with small amount of numbers:

command:
`java -jar Task-1.0-jar-with-dependencies.jar task1 20 10` //it generates 20 numbers in range 0-10

output:
```
0 1 2 4 5 6 7 8 9 10
count: 20
distinct: 10
min: 0
max: 10
```

### Task2 ###
As in the previous task, numbers are generated automatically and here are some optional parameters too.
* To run this task just execute `java -jar Task-1.0-jar-with-dependencies.jar task2` terminal command.
* There are three more optional arguments expected:


	Number Count - How many numbers should be passed to task - default: 100000
	Number Range - Generated numbers are in range 0<=x<=thisParamValue - default: 500
	Expected sum - Target sum - default: 501
* Example with small amount of numbers:

command:
`java -jar Task-1.0-jar-with-dependencies.jar task2 20 10 13` //it generates 20 numbers in range 0-10 and tries to find sum: 13 

output:
```
3 10
4 9
4 9
4 9
6 7
6 7
```


### Task3 ###
This task is a little different than previous. 
* To run this task just execute `java -jar Task-1.0-jar-with-dependencies.jar task3`.
* Next you have to input edge count and input each edge separately by passing two vertexes splitted by space.

* Example:

command:
`java -jar Task-1.0-jar-with-dependencies.jar task3` 

input:
```
5
0 1
1 2
2 3
3 4
6 7
```
output:
```
2
```